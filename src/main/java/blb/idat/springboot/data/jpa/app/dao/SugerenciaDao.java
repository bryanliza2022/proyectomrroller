package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;


import blb.idat.springboot.data.jpa.app.model.Sugerencia;

public interface SugerenciaDao {

	//VICTORIA 
	
	//CRUD LISTAR CREAR EDITAR ELIMINAR
	
	public List<Sugerencia>findAll();
	
	public void guardar(Sugerencia sugerencia);

	public Sugerencia editar(Long id);
	
	public void eliminar(Long id);
}
