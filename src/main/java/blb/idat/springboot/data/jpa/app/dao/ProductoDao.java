package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import blb.idat.springboot.data.jpa.app.model.Producto;

public interface ProductoDao {

	
	//CRUD LISTAR ACTUALIZAR EDITAR Y GUARDAR
	
    public List<Producto> findAll();
	
	public void guardar (Producto producto);
	
	public Producto editar(Long id);
	
	public void eliminar(Long id);
}
