package blb.idat.springboot.data.jpa.app.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="tbcotizaciones")
public class Cotizacion implements Serializable {

public static final long serialVersionUID=1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="tbc_medidaalto")
	private double medidaalto;
	
	@Column(name="tbc_medidaancho")
	private double medidaancho;
	
	@Column(name="tbc_producto")
	private String nomproducto;
	
	@Column(name="tbc_dolar")
	private String dolar;
	
	@Column(name="tbc_soles")
	private String soles;
	
	@Column(name="tbc_fecha")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fecha;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getMedidaalto() {
		return medidaalto;
	}

	public void setMedidaalto(double medidaalto) {
		this.medidaalto = medidaalto;
	}

	public double getMedidaancho() {
		return medidaancho;
	}

	public void setMedidaancho(double medidaancho) {
		this.medidaancho = medidaancho;
	}


	public String getDolar() {
		return dolar;
	}

	public void setDolar(String dolar) {
		this.dolar = dolar;
	}

	public String getSoles() {
		return soles;
	}

	public void setSoles(String soles) {
		this.soles = soles;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNomproducto() {
		return nomproducto;
	}

	public void setNomproducto(String nomproducto) {
		this.nomproducto = nomproducto;
	}

	
	
}
