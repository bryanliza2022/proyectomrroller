package blb.idat.springboot.data.jpa.app.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import blb.idat.springboot.data.jpa.app.dao.ClienteDao;
import blb.idat.springboot.data.jpa.app.model.Cliente;

@Controller
public class ClienteController {

	@Autowired
	private ClienteDao clienteDao;

	@GetMapping(value = "/administrador")
	public String listarCliente(Model model) {

		model.addAttribute("clientes", clienteDao.findAll());
		return "administrador";
	}

	@GetMapping(value = "/registrarse")
	public String crear(Model model) {
		Cliente cliente = new Cliente();
		model.addAttribute("cliente", cliente);
		return "registrarse";
	}

	@PostMapping(value = "/registrarse")
	public String almacenar(Cliente cliente) {
		clienteDao.guardar(cliente);
		return "redirect:/";
	}

	@GetMapping(value = "/ver/{id}")
	public String verCliente(@PathVariable(value = "id") Long id, Model model) {

		Cliente cliente = clienteDao.editarCliente(id);
		if (cliente == null) {
			return "redirect:/listar";
		}
		model.addAttribute("cliente", cliente);
		model.addAttribute("titulo", "detalle del cliente: " + cliente.getNombre());

		return "ver";

	}

	@GetMapping(value = "/form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		Cliente cliente = null;
		if (id > 0) {
			cliente = clienteDao.editarCliente(id);
		} else {
			return "redirect:/listar";
		}
		model.put("titulo", "Editar Cliente");
		model.put("cliente", cliente);
		return "form";
	}

	@GetMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id) {
		if (id > 0) {
			clienteDao.eliminarCliente(id);
		}

		return "redirect:/listar";
	}

}
