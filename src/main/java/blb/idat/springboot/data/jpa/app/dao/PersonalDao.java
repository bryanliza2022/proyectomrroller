package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import blb.idat.springboot.data.jpa.app.model.Personal;

public interface PersonalDao {
	
	public List<Personal> findAll();
	
	public void guardar (Personal personal);
	
	public Personal editar(Long id);
	
	public void eliminar(Long id);
}
