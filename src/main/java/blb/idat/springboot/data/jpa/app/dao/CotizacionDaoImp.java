package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import blb.idat.springboot.data.jpa.app.model.Cotizacion;

@Repository
public class CotizacionDaoImp implements CotizacionDao {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<Cotizacion> findAll() {
		
		return em.createQuery("from Cotizacion").getResultList();
	}

	@Transactional
	@Override
	public void guardar(Cotizacion cotizacion) {
		
		if(cotizacion.getId()!=null && cotizacion.getId()>0){
			em.merge(cotizacion);
		}else {
			em.persist(cotizacion);
		}
		
	}

	@Transactional
	@Override
	public Cotizacion editar(Long id) {
		
		return em.find(Cotizacion.class, id);
	}

	@Transactional
	@Override
	public void eliminar(Long id) {
		
		em.remove(editar(id));
		
	}

}
