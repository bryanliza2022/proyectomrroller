package blb.idat.springboot.data.jpa.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {
	
	@RequestMapping()
	public String home(){

		 return "home";
	}
	
	@RequestMapping("/login")
	public String login(){
		return "login";
	}

	@RequestMapping("/administrador")
	public String administrador(){
		return "administrador";
	}
	
	
	@RequestMapping("/listarsugerencia")
	public String verSugerencia(){
		return "listarSugerencia";
	}
	
	@RequestMapping("/productos")
	public String productos(){
		return "productos";
	}
	
	
	
}	
