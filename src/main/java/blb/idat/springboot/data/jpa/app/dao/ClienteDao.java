package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;


import blb.idat.springboot.data.jpa.app.model.Cliente;

public interface ClienteDao {

	//CRUD LISTAR-GUARDAR-EDITAR-ELIMINAR
	
	public List<Cliente>findAll();
	
	public void guardar(Cliente cliente);

	public Cliente editarCliente(Long id);
	
	public void eliminarCliente(Long id);

}
