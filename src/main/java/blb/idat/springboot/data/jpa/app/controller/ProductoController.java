package blb.idat.springboot.data.jpa.app.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;



public class ProductoController {

	/*
	 * //Necesito traer el método Listar:
	 * 
	 * @Autowired private ProductoDao productoDao;
	 * 
	 * //Métodos:
	 * 
	 * @GetMapping(value="/listar") public String listarClientes(Model model) {
	 * model.addAttribute("titulo","Listado de Producto");
	 * model.addAttribute("clientes",clienteDao.findAll()); return "listar"; }
	 * 
	 * //Formulario [GET-POST]
	 * 
	 * @GetMapping(value="/form") public String crear(Model model) { Producto
	 * cliente=new Cliente(); model.addAttribute("titulo","Formulario de Producto");
	 * model.addAttribute("Producto",Producto); return "form"; }
	 * 
	 * @PostMapping(value="/form") public String guardar(Producto Producto,
	 * 
	 * @RequestParam("file") MultipartFile foto) {
	 * 
	 * if(!foto.isEmpty()){ Path
	 * directorioImagenes=Paths.get("src//main//resources//static/uploads"); String
	 * rootPath = directorioImagenes.toFile().getAbsolutePath(); try { byte[] bytes
	 * = foto.getBytes(); Path
	 * rutaCompleta=Paths.get(rootPath+"//"+foto.getOriginalFilename());
	 * Files.write(rutaCompleta, bytes);
	 * cliente.setFoto(foto.getOriginalFilename());
	 * 
	 * } catch (IOException e) {
	 * 
	 * e.printStackTrace(); } }
	 * 
	 * 
	 * clienteDao.save(Producto); return "redirect:listar"; }
	 * 
	 * @GetMapping(value="/ver/{id}") public String
	 * verImagen(@PathVariable(value="id") Long id,Model model) {
	 * 
	 * Producto Producto = clienteDao.editarCliente(id); if(Producto==null){ return
	 * "redirect:/listar"; } model.addAttribute("cliente",Producto);
	 * model.addAttribute("titulo","detalle del cliente: "+cliente.getNombre());
	 * 
	 * return "ver";
	 * 
	 * }
	 * 
	 * 
	 * 
	 * @GetMapping(value="/form/{id}") public String
	 * editar(@PathVariable(value="id") Long id, Map<String, Object> model) {
	 * Cliente cliente=null; if(id>0) { cliente=clienteDao.editarCliente(id); } else
	 * { return "redirect:/listar"; } model.put("titulo","Editar Producto");
	 * model.put("cliente", cliente); return "form"; }
	 * 
	 * @GetMapping(value="/eliminar/{id}") public String
	 * eliminar(@PathVariable(value="id") Long id) { if(id>0) {
	 * clienteDao.eliminarCliente(id); }
	 * 
	 * return "redirect:/listar"; }
	 */
	
}
