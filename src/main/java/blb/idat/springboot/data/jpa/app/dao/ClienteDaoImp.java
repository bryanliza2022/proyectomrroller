package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import blb.idat.springboot.data.jpa.app.model.Cliente;

@Repository
public  class ClienteDaoImp implements ClienteDao {

	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<Cliente> findAll() {
		
		return em.createQuery("from Cliente").getResultList();
	}
	
	@Transactional
	@Override
	public void guardar(Cliente cliente) {
		if(cliente.getId()!=null && cliente.getId()>0){
			em.merge(cliente);
		}else {
			em.persist(cliente);
		}
	}
	
	@Transactional
	@Override
	public Cliente editarCliente(Long id) {
		
		return em.find(Cliente.class, id);
	}
	
	@Transactional
	@Override
	public void eliminarCliente(Long id) {
		
		em.remove(editarCliente(id));
		
	}
	


	

}
