package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import blb.idat.springboot.data.jpa.app.model.Cotizacion;


public interface CotizacionDao {

	public List<Cotizacion> findAll();
	
	public void guardar (Cotizacion cotizacion);
	
	public Cotizacion editar(Long id);
	
	public void eliminar(Long id);
}
