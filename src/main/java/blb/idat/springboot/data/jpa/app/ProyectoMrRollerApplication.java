package blb.idat.springboot.data.jpa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoMrRollerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoMrRollerApplication.class, args);
	}

}
