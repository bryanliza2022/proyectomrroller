package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import blb.idat.springboot.data.jpa.app.model.Producto;

@Repository
public class ProductoDaoImp implements ProductoDao {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional (readOnly=true)
	@Override
	public List<Producto> findAll() {
		
		return em.createQuery("from Producto").getResultList();
	}

	@Transactional
	@Override
	public void guardar(Producto producto) {
		
		if(producto.getId()!=null && producto.getId()>0){
			em.merge(producto);
		}else {
			em.persist(producto);
		}
		
	}

	@Transactional
	@Override
	public Producto editar(Long id) {
		
		return em.find(Producto.class, id);
	}

	@Transactional
	@Override
	public void eliminar(Long id) {
		
		em.remove(editar(id));
		
	}

}
