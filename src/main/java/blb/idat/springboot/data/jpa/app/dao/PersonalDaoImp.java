package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import blb.idat.springboot.data.jpa.app.model.Personal;


@Repository
public class PersonalDaoImp implements PersonalDao {

	//CRUD LISTAR GUARDAR EDITAR ELIMINAR
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<Personal> findAll() {
		
		return em.createQuery("from Producto").getResultList();
	}

	@Transactional
	@Override
	public void guardar(Personal personal) {
		
		if(personal.getId()!=null && personal.getId()>0){
			em.merge(personal);
		}else {
			em.persist(personal);
		}
		
	}

	@Transactional
	@Override
	public Personal editar(Long id) {
		
		return em.find(Personal.class, id);
	}

	@Transactional
	@Override
	public void eliminar(Long id) {
		
		em.remove(editar(id));
	}

	
}
