package blb.idat.springboot.data.jpa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import blb.idat.springboot.data.jpa.app.dao.SugerenciaDao;
import blb.idat.springboot.data.jpa.app.model.Sugerencia;

@Controller
public class SugerenciaController {

	@Autowired
	private SugerenciaDao sugerenciaDao;

	@GetMapping(value = "/listarsugerencia")
	public String listarSugerencia(Model model) {

		model.addAttribute("sugerencias", sugerenciaDao.findAll());

		return "listarsugerencia";
	}

	@GetMapping(value = "/")
	public String crear(Model model) {

		Sugerencia sugerencia = new Sugerencia();
		model.addAttribute("sugerencia", sugerencia);

		return "home";
	}

	@PostMapping(value = "/home")
	public String almacenar(Sugerencia sugerencia) {
		sugerenciaDao.guardar(sugerencia);
		return "redirect:/";
	}

	@GetMapping(value = "/ver/id")
	public String verSugerencia(@PathVariable(value = "id") Long id, Model model) {
		Sugerencia sugerencia = sugerenciaDao.editar(id);

		if (sugerencia == null) {
			return "redirect:/administrador";
		}
		model.addAttribute("sugerencia", sugerencia);

		return "ver";
	}

}
