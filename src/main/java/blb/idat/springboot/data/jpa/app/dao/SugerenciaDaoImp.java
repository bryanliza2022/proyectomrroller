package blb.idat.springboot.data.jpa.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import blb.idat.springboot.data.jpa.app.model.Sugerencia;

@Repository
public class SugerenciaDaoImp implements SugerenciaDao {

	
	
	@PersistenceContext //Administrador de entidades 
	private EntityManager em;  //Administrador de entidades 

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Sugerencia> findAll() {

		return em.createQuery("from Sugerencia").getResultList();
	}

	@Transactional
	@Override
	public void guardar(Sugerencia sugerencia) {

		if (sugerencia.getId() != null && sugerencia.getId() > 0) {
			em.merge(sugerencia);
		} else {
			em.persist(sugerencia);
		}

	}

	@Transactional
	@Override
	public Sugerencia editar(Long id) {

		return em.find(Sugerencia.class, id);
	}

	@Transactional
	@Override
	public void eliminar(Long id) {

		em.remove(editar(id));
	}

}
